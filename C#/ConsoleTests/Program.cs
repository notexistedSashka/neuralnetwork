﻿using System;
using System.Collections.Generic;
using NeuralLib;

namespace ConsoleTests
{
    class Program
    {
        static void Main(string[] args)
        {
            var dataset = new List<Tuple<double, List<double>>>
            {
                //new Tuple<double, List<double>>(1, new List<double> { 0, 0 } ),
                //new Tuple<double, List<double>>(1, new List<double> { 0, 1 } ),
                //new Tuple<double, List<double>>(1, new List<double> { 1, 0 } ),
                //new Tuple<double, List<double>>(0, new List<double> { 1, 1 } )

                //new Tuple<double, List<double>>(0, new List<double> { 0, 0, 0, 1} ),
                //new Tuple<double, List<double>>(0, new List<double> { 0, 0, 0, 0} ),
                //new Tuple<double, List<double>>(1, new List<double> { 0, 0, 1, 0} ),
                //new Tuple<double, List<double>>(0, new List<double> { 0, 0, 1, 1} ),
                //new Tuple<double, List<double>>(0, new List<double> { 0, 1, 0, 0} ),
                //new Tuple<double, List<double>>(0, new List<double> { 0, 1, 0, 1} ),
                //new Tuple<double, List<double>>(1, new List<double> { 0, 1, 1, 0} ),
                //new Tuple<double, List<double>>(0, new List<double> { 0, 1, 1, 1} ),
                //new Tuple<double, List<double>>(1, new List<double> { 1, 0, 0, 0} ),
                //new Tuple<double, List<double>>(1, new List<double> { 1, 0, 0, 1} ),
                //new Tuple<double, List<double>>(1, new List<double> { 1, 0, 1, 0} ),
                //new Tuple<double, List<double>>(1, new List<double> { 1, 0, 1, 1} ),
                //new Tuple<double, List<double>>(1, new List<double> { 1, 1, 0, 0} ),
                //new Tuple<double, List<double>>(0, new List<double> { 1, 1, 0, 1} ),
                //new Tuple<double, List<double>>(1, new List<double> { 1, 1, 1, 0} ),
                //new Tuple<double, List<double>>(1, new List<double> { 1, 1, 1, 1} )

                //                                                    S  A  F  T  P
                new Tuple<double, List<double>>(0, new List<double> { 1, 1, 1, 0, 0} ),
                new Tuple<double, List<double>>(0, new List<double> { 0, 1, 1, 0, 1} ),
                new Tuple<double, List<double>>(1, new List<double> { 1, 0, 0, 1, 1} ),
                new Tuple<double, List<double>>(1, new List<double> { 1, 0, 0, 1, 0} ),
                new Tuple<double, List<double>>(0, new List<double> { 0, 0, 1, 0, 0.5} ),
                new Tuple<double, List<double>>(1, new List<double> { 1, 1, 0, 1, 1} ),
                new Tuple<double, List<double>>(1, new List<double> { 1, 1, 1, 1, 0.5} ),
                new Tuple<double, List<double>>(1, new List<double> { 0, 0, 0, 1, 1} ),
                new Tuple<double, List<double>>(1, new List<double> { 1, 0, 0, 0, 0.5} ),
                new Tuple<double, List<double>>(0, new List<double> { 1, 1, 1, 0, 1} )
            };


            var topology = new Topology(5, new List<int> { 10 }, 1, 0.1);
            var neuralNetwork = new NeuralNetwork(topology);

            var difference = neuralNetwork.Learn(dataset, 400000);
            Console.WriteLine(difference);


            foreach (var item in dataset)
            {
                var output = neuralNetwork.FeedForward(item.Item2).Output;
                Console.WriteLine($"Actual round: { Math.Round(output) } Expected: {item.Item1} Correct: { Math.Round(output) == item.Item1 } Actual: { output }");
            }

            var expected = 0;
            var val = neuralNetwork.FeedForward(new List<double> { 0, 0, 0, 0, 0 }).Output;
            Console.WriteLine($"Actual round: { Math.Round(val) } Expected: {expected} Correct: { Math.Round(val) == expected } Actual: { val }");
        }
    }
}
