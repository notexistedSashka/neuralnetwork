﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NeuralLib
{
    public class NeuralNetwork
    {
        public Topology Topology { get; }
        public List<Layer> Layers { get; }

        public NeuralNetwork(Topology topology)
        {
            Topology = topology;

            Layers = new List<Layer>();

            CreateInputLayer();
            CreateHiddenLayers();
            CreateOutputLayer();
        }

        public Neuron FeedForward(List<double> inputs)
        {
            if (inputs.Count != Topology.InputLayerCount)
                throw new Exception("NeuralNetwork::FeedForward inputs.Count != Topology.InputLayerCount");

            for (int i = 0; i < inputs.Count; i++)
            {
                var signals = new List<double> { inputs[i] };
                Layers[0].Neurons[i].FeedForward(signals);
            }

            for (int i = 1; i < Layers.Count; i++)
            {
                var signals = Layers[i - 1].GetOutputs();

                foreach (var item in Layers[i].Neurons)
                    item.FeedForward(signals);
            }

            return Layers.Last().Neurons.OrderByDescending(n => n.Output).First();
        }

        public double Learn(List<Tuple<double, List<double>>> dataset, int epochCount)
        {
            double error = 0;

            for (int i = 0; i < epochCount; i++)
            {
                foreach (var data in dataset)
                {
                    error += Backpropagation(data.Item1, data.Item2);
                }
            }

            return error / epochCount;
        }

        private double Backpropagation(double expected, List<double> inputs)
        {
            var actual = FeedForward(inputs).Output;

            var difference = actual - expected;

            foreach (var neuron in Layers.Last().Neurons)
                neuron.Learn(difference, Topology.LearningRate);

            for (int i = Layers.Count - 2; i > 0; i--)
            {
                var previousLayer = Layers[i + 1];
                var currentLayer = Layers[i];

                for (int j = 0; j < currentLayer.NeuronsCount; j++)
                {
                    var currentNeuron = currentLayer.Neurons[j];

                    for (int a = 0; a < previousLayer.NeuronsCount; a++)
                    {
                        var previousNeuron = previousLayer.Neurons[a];
                        var error = previousNeuron.Weights[j] * previousNeuron.Delta;

                        currentNeuron.Learn(error, Topology.LearningRate);
                    }
                }
            }

            return difference * difference;
        }

        #region CreateLayers

        private void CreateInputLayer()
        {
            var layer = new List<Neuron>();
            for (int i = 0; i < Topology.InputLayerCount; i++)
                layer.Add(new Neuron(1, NeuronType.Input));

            Layers.Add(new Layer(layer, NeuronType.Input));
        }

        private void CreateHiddenLayers()
        {
            for (int j = 0; j < Topology.HiddenLayersCounts.Count; j++)
            {
                var layer = new List<Neuron>();
                var lastLayer = Layers.Last();
                for (int i = 0; i < Topology.HiddenLayersCounts[j]; i++)
                    layer.Add(new Neuron(lastLayer.NeuronsCount));

                Layers.Add(new Layer(layer));
            }
        }

        private void CreateOutputLayer()
        {
            var layer = new List<Neuron>();
            var lastLayer = Layers.Last();
            for (int i = 0; i < Topology.OutputLayerCount; i++)
                layer.Add(new Neuron(lastLayer.NeuronsCount, NeuronType.Output));

            Layers.Add(new Layer(layer, NeuronType.Output));
        }

        #endregion
    }
}
