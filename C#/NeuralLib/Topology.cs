﻿using System.Collections.Generic;

namespace NeuralLib
{
    public class Topology
    {
        public int InputLayerCount { get; }
        public int OutputLayerCount { get; }
        public List<int> HiddenLayersCounts { get; }

        public double LearningRate { get; }

        public Topology(int inputLayerCount, List<int> hiddenLayersCounts, int outputLayerCount, double learningRate)
        {
            InputLayerCount = inputLayerCount;
            HiddenLayersCounts = hiddenLayersCounts;
            OutputLayerCount = outputLayerCount;
            LearningRate = learningRate;
        }
    }
}
