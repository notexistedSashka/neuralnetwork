﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;

namespace NeuralLib
{
    public class Neuron
    {
        public List<double> Weights { get; }
        public List<double> Inputs { get; }
        public NeuronType NeuronType { get; }
        public double Output { get; private set; }
        public double Delta { get; private set; }

        // for tanh
        public double Gradient { get; private set; }

        public Neuron(int inputCount, NeuronType neuronType = NeuronType.Hidden)
        {
            NeuronType = neuronType;

            Inputs = new List<double>();
            Weights = new List<double>();
            WeightInitialize(inputCount);
        }

        private void WeightInitialize(int inputCount)
        {
            var rand = new Random();
            for (int i = 0; i < inputCount; i++)
            {
                if (NeuronType != NeuronType.Input)
                    Weights.Add(rand.NextDouble());
                else
                    Weights.Add(1);

                Inputs.Add(0);
            }
        }

        public double FeedForward(List<double> inputs)
        {
            if (inputs.Count != Weights.Count)
                throw new Exception("Neuron::FeedForward inputs.Count != Weight.Count");

            double sum = 0d;
            for (int i = 0; i < inputs.Count; i++)
            {
                sum += inputs[i] * Weights[i];
                Inputs[i] = inputs[i];
            }

            if(NeuronType != NeuronType.Input)
                Output = ActivationFunction(sum);
            else
                Output = sum;

            return Output;
        }

        private double ActivationFunction(double x)
        {
            //return 1d / (1d + Math.Exp(-x));
            return Math.Tanh(x);
        }

        private double ActivationFunctionDx(double x)
        {
            //var s = ActivationFunction(x);
            //return s / (1 - s);
            return 1d - x * x;
        }

        // for sigmoid
        public void Learn(double error, double learningRate)
        {
            Delta = error * ActivationFunctionDx(Output);

            for (int i = 0; i < Weights.Count; i++)
                Weights[i] = Weights[i] - Inputs[i] * Delta * learningRate;
        }

        // for tanh
        public void LearnOutput(double targetValue)
        {
            double learningRate = 0.15;
            double alpha = 0.5;

            Delta = targetValue - Output;
            Gradient = Delta * ActivationFunctionDx(Output);

            for (int i = 0; i < Weights.Count; i++)
                Weights[i] = learningRate * Output * Gradient + alpha * Weights[i];
        }

        // for tanh
        public void LearnHidden(Layer layer)
        {
            double learningRate = 0.15;
            double alpha = 0.5;

            double sum = 0.0;
            for (int i = 0; i < layer.NeuronsCount; i++)
            {
                sum += Weights[i] * layer.Neurons[i].Gradient;
            }

            double gradient = sum * ActivationFunctionDx(Output);

            for (int i = 0; i < Weights.Count; i++)
                Weights[i] = learningRate * Output * gradient + alpha * Weights[i];
        }
    }
}
