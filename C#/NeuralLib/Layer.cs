﻿using System;
using System.Collections.Generic;

namespace NeuralLib
{
    public class Layer
    {
        public List<Neuron> Neurons { get; }
        public NeuronType NeuronType { get; }
        public int NeuronsCount => Neurons?.Count ?? 0;

        public Layer(List<Neuron> neurons, NeuronType neuronType = NeuronType.Hidden)
        {
            for (int i = 0; i < neurons.Count; i++)
                if (neurons[i].NeuronType != neuronType)
                    throw new Exception($"Layer::ctor neurons[{i}] have not correct neuron type");

            Neurons = neurons;
            NeuronType = neuronType;
        }

        public List<double> GetOutputs()
        {
            var result = new List<double>();

            foreach (var item in Neurons)
                result.Add(item.Output);

            return result;
        }
    }
}
