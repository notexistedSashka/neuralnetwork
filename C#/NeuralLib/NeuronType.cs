﻿namespace NeuralLib
{
    public enum NeuronType
    {
        Input,
        Hidden,
        Output
    }
}
