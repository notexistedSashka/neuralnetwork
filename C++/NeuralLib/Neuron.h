#pragma once
#include <vector>
#include "NeuronType.h"

class Neuron
{
public:
	Neuron();
	~Neuron();

	//void set_weights(std::vector<double>);
	std::vector<double> get_weights();

	//void set_type(NeuronType);
	NeuronType get_type();
private:
	std::vector<double> _weights;
	NeuronType _type;

};