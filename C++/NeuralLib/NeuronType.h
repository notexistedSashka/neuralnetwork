#pragma once

enum NeuronType
{
	Input,
	Hidden,
	Output
};