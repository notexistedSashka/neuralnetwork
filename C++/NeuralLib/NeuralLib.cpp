﻿#include <iostream>
#include <vector>
#include "Neuron.h"

int main()
{
    auto neuron = new Neuron();
    std::vector<double> vec;

    vec.push_back(1.1);
    vec.push_back(1.2);
    vec.push_back(2.2);
    vec.push_back(2.3);
    vec.push_back(3.3);
    vec.push_back(3.4);

    neuron->set_weights(vec);

    auto tmp = neuron->get_weights();
    int count = vec.size();

    for (size_t i = 0; i < count; i++)
    {
        std::cout << vec[i] << " ";
    }

    std::cout << std::endl;

    for (size_t i = 0; i < count; i++)
    {
        std::cout << tmp[i] << " ";
    }
}
