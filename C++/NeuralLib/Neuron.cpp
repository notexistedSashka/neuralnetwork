#include <vector>
#include "Neuron.h"

Neuron::Neuron()
{
}

Neuron::~Neuron()
{
}

void Neuron::set_weights(std::vector<double> weights)
{
	for (size_t i = 0; i < weights.size(); i++)
		_weights.push_back(weights[i]);
}

std::vector<double> Neuron::get_weights()
{
	return _weights;
}

void Neuron::set_type(NeuronType type)
{
	_type = type;
}

NeuronType Neuron::get_type()
{
	return _type;
}
